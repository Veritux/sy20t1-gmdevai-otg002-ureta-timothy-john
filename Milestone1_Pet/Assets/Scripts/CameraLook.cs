﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLook : MonoBehaviour
{
    [SerializeField] private float lookSensitiviy = 100f;
    [SerializeField] private Transform player;
    [SerializeField] private float lookRange;
    float xRotation = 0f; // rotation about the X axis (Up or Dowwn rotation)

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // Prevents the cursor from moving off the screen
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Using the X and Y axes as input 
        float mouseX = Input.GetAxis("Mouse X") * lookSensitiviy * Time.deltaTime; 
        float mouseY = Input.GetAxis("Mouse Y") * lookSensitiviy * Time.deltaTime;

        // Rotate the player body if we are looking left or right. Rotation about the Y axis. 
        player.Rotate(Vector3.up *mouseX); // Vector3.up is like a normalized value. Long method would probably be to create a new Vector and give it a value

      

        xRotation -= mouseY; // if it's +=, the direction is somehow reversed
        xRotation = Mathf.Clamp(xRotation, -(lookRange), lookRange); //Clamp to prevent overshooting 
        //player.Rotate(Vector3.right * mouseY);

        // Rotate the camera and the player
        transform.localRotation = Quaternion.Euler(xRotation, 0, 0); // .Euler rotates x value around the x axis, y value around y axis etc
        player.localRotation = Quaternion.Euler(xRotation, 0, 0);
    }
}
