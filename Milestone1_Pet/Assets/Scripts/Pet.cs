﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour
{
    [SerializeField] private Transform goal;
    [SerializeField] private float speed; 
    [SerializeField] private float rotSpeed;
    [SerializeField] private float closestDistToGoal;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 goalLocation = new Vector3(goal.position.x, this.transform.position.y, goal.position.z); // We don't want the Pet to float in the air
        Vector3 direction = goalLocation - this.transform.position;

        // Lerping
        if (Vector3.Distance(goalLocation, this.transform.position) > closestDistToGoal)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, goalLocation, Time.deltaTime * speed);
        }

        // Slerping
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);
    }
}
