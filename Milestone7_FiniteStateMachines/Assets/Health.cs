﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float maxHealth;
    private GameObject owner; 
    private float currentHealth;

    public float GetPercCurrentHealth()
    {
        return (currentHealth / maxHealth) * 100;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    //void Init(GameObject nOwner)
    //{
    //    owner = nOwner;
    //}

    public void TakeDamage(float value)
    {
        if (this != null)
        {
            currentHealth -= value;
            Debug.Log("took damage");

            // Check if <= 0
            if (currentHealth <= 0)
            {
                Debug.Log("GameObject has died!");
                Destroy(this.gameObject);
            }
        }
    }
}
