﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flee : NPCBaseFSM
{
    private float speedModifier = 2.5f;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Flee state!");
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Flee
        Vector3 direction = (opponent.transform.position - NPC.transform.position).normalized;
        NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation,
                                            Quaternion.LookRotation(-direction),
                                            rotSpeed * Time.deltaTime);
        NPC.transform.Translate(0, 0, Time.deltaTime * speed * speedModifier);
    }
}
