﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    GameObject[] goalLocations;
    NavMeshAgent agent;
    Animator animator;
    float speedMultiplier;
    float detectionRadius = 15;
    float fleeRadius = 10;

    void ResetAgent()
    {
        speedMultiplier = Random.Range(0.1f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120;
        animator.SetFloat("speedMultiplier", speedMultiplier);
        animator.SetTrigger("isWalking");
        agent.ResetPath();
    }

    // Start is called before the first frame update
    void Start()
    {
        goalLocations = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();

        agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        animator.SetTrigger("isWalking");
        animator.SetFloat("wOffset", Random.Range(0.1f, 1));

        speedMultiplier = Random.Range(0.1f, 1.5f);
        agent.speed = 2 * speedMultiplier; // movement speed multiplier
        animator.SetFloat("speedMultiplier", speedMultiplier); // animation speed
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance < 1)
        {
            agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        }
    }

    public void DetectNewObstacle(Vector3 location, GameObject obstacle)
    {
        if (Vector3.Distance(location, this.transform.position) < detectionRadius)
        {
            Vector3 newGoal = Vector3.zero;
            NavMeshPath path = new NavMeshPath();

            if (obstacle.gameObject.CompareTag("flockObstacle"))
            {
                newGoal = Flock(location);
            }
            else if (obstacle.gameObject.CompareTag("fleeObstacle"))
            {
                newGoal = Flee(location);
            }

            agent.CalculatePath(newGoal, path);

            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]); // latest created path 
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }

    private Vector3 Flee(Vector3 location)
    {
        Vector3 fleeDirection = (this.transform.position - location).normalized;
        Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;
        return newGoal;
    }

    private Vector3 Flock(Vector3 location)
    {
        Vector3 flockDirection = (this.transform.position - location).normalized;
        Vector3 newGoal = this.transform.position - flockDirection * fleeRadius;
        return newGoal;
    }
}
