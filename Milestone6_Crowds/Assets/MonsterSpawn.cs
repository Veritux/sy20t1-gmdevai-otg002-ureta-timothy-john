﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawn : MonoBehaviour
{
    public GameObject fleeObstacle;
    public GameObject flockObstacle;
    GameObject[] agents;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()   
    {
        if (Input.GetMouseButtonDown(0))
        {
            SpawnObstacle(fleeObstacle);
        }
        else if (Input.GetMouseButton(1))
        {
            SpawnObstacle(flockObstacle);
        }
    }

    private void SpawnObstacle(GameObject obstacle)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction, out hit))
        {
            GameObject newObstacle = Instantiate(obstacle, hit.point, obstacle.transform.rotation);
            foreach (GameObject a in agents)
            {
                a.GetComponent<AIControl>().DetectNewObstacle(hit.point, newObstacle);
            }
        }
    }
}
