﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hider : Enemy
{
    protected override void EnemyDetectedBehavior()
    {
        if (aiController.CanSeeTarget())
        {
            aiController.CleverHide();
        }
    }
}
