﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evader : Enemy
{
    protected override void EnemyDetectedBehavior()
    {
        aiController.Evade();
    }
}
