﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected AIControl aiController;
    [SerializeField] private string name;
    private bool targetInRange = false;

    // Update is called once per frame
    void Update()
    {
        if (targetInRange != true)
        {
            Wander();
        }
        else
        {
            EnemyDetectedBehavior();
        }
    }

    private void Wander()
    {
        aiController.Wander();
    }

    protected virtual void EnemyDetectedBehavior()
    {

    }

    // Accessed thru EnemyDetectionZone.cs
    public void EnemyDetected()
    {
        Debug.Log(name + " has detected Player!");
        targetInRange = true;
    }

    public void EnemyOutOfRange()
    {
        Debug.Log("Player has exited " + name + "'s detection range");
        targetInRange = false;
    }
}
