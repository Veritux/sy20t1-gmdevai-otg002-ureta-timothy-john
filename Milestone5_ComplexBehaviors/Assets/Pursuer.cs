﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pursuer : Enemy
{
    protected override void EnemyDetectedBehavior()
    {
        aiController.Pursuit();
    }
}
