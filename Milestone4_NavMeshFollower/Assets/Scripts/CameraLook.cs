﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLook : MonoBehaviour
{
    [SerializeField] private float lookSensitiviy = 100f;
    [SerializeField] private Transform player;
    [SerializeField] private float lookRange;
    float xRotation = 0f; 

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; 
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //// Using the X and Y axes as input 
        float mouseX = Input.GetAxis("Mouse X") * lookSensitiviy * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * lookSensitiviy * Time.deltaTime;

        player.Rotate(Vector3.up * mouseX);

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -(lookRange), lookRange);

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
    }
}
