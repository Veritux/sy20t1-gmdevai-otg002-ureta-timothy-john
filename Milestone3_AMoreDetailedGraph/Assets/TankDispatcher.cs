﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankDispatcher : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI tankStatus;
    [SerializeField] private List<GameObject> tankList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tankStatus.text = "Tanks\n" + tankList[0].GetComponent<FollowPath>().getName() + ": " + tankList[0].GetComponent<FollowPath>().getStatus()
                             + "\n" + tankList[1].GetComponent<FollowPath>().getName() + ": " + tankList[1].GetComponent<FollowPath>().getStatus()
                             + "\n" + tankList[2].GetComponent<FollowPath>().getName() + ": " + tankList[2].GetComponent<FollowPath>().getStatus();
    }

    public void GoToHelipad()
    {
        foreach (GameObject tank in tankList)
        {
           tank.GetComponent<FollowPath>().GoToHelipad();
        }
    }

    public void GoToRuins()
    {
        foreach (GameObject tank in tankList)
        {
           tank.GetComponent<FollowPath>().GoToRuins();
        }
    }

    public void GoToFactory()
    {
        foreach (GameObject tank in tankList)
        {
          tank.GetComponent<FollowPath>().GoToFactory();
        }
    }
    public void GoToTwinMountains()
    {
        foreach (GameObject tank in tankList)
        {
           tank.GetComponent<FollowPath>().GoToTwinMountains();
        }
    }
    public void GoToBarracks()
    {
        foreach (GameObject tank in tankList)
        {
           tank.GetComponent<FollowPath>().GoToBarracks();
        }
    }
    public void GoToCommandCenter()
    {
        foreach (GameObject tank in tankList)
        {
           tank.GetComponent<FollowPath>().GoToCommandCenter();
        }
    }
    public void GoToOilPumps()
    {
        foreach (GameObject tank in tankList)
        {
            tank.GetComponent<FollowPath>().GoToOilPumps();
        }
    }
    public void GoToTankers()
    {
        foreach (GameObject tank in tankList)
        {
             tank.GetComponent<FollowPath>().GoToTankers();
        }
    }
    public void GoToRadar()
    {
        foreach (GameObject tank in tankList)
        {
             tank.GetComponent<FollowPath>().GoToRadar();
        }
    }
    public void GoToCommandPost()
    {
        foreach (GameObject tank in tankList)
        {
             tank.GetComponent<FollowPath>().GoToCommandPost();
        }
    }
    public void GoToMiddleMap()
    {
        foreach (GameObject tank in tankList)
        {
             tank.GetComponent<FollowPath>().GoToMiddleMap();
        }
    }
}
