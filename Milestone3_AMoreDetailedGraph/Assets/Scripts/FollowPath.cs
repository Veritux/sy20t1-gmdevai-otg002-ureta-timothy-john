﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    [SerializeField] private string tankName;
    [SerializeField] private Transform goal;
    [SerializeField] private float speed = 5.0f;
    [SerializeField] private float accuracy = 1.0f;
    [SerializeField] private float rotSpeed = 2.0f;
    [SerializeField] private GameObject wpManager;
    
    private bool isActive = false;
    private GameObject[] wps;
    private GameObject currentNode;
    private int currentWaypointIndex = 0;
    private Graph graph;
    private Rigidbody rigidBody;

    public string getName()
    {
        return tankName;
    }

    public string getStatus()
    {
        if (isActive == true) { return "Active"; }
        else { return "Disabled";  }

    }

    public void changeStatus()
    {
        if (isActive == true)
        {
            isActive = false;
        }
        else
        {
            isActive = true;
        }
        Debug.Log("Status changed, status is now: " + getStatus());
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody>();
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent <WaypointManager>().graph;
        currentNode = wps[30]; // This was 0 previously. So that's why the tank kept on going to that location on the map first before everything
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (isActive == true) // Will only do stuff if activated 
        {
            if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
            {
                return;
            }

            // The node we are closest to at the moment
            currentNode = graph.getPathPoint(currentWaypointIndex);

            // If we are close enough to the current waypoint, move to the next one
            if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position,
                                transform.position) < accuracy)
            {
                currentWaypointIndex++;
            }

            // If we are not at the end of the path
            if (currentWaypointIndex < graph.getPathLength())
            {
                goal = graph.getPathPoint(currentWaypointIndex).transform;
                Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
                Vector3 direction = lookAtGoal - this.transform.position;
                //this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                //                                            Quaternion.LookRotation(direction),
                //                                            Time.deltaTime * rotSpeed);
                rigidBody.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                           Quaternion.LookRotation(direction),
                                                           Time.deltaTime * rotSpeed);
                //this.transform.Translate(0, 0, speed * Time.deltaTime);
                rigidBody.velocity = transform.forward * speed * Time.deltaTime;
            }
            else // stop rigidbody shenanigans. Sir, i don't know how to stop them if they have just stopped and a collision forces them to move around 
            {
                rigidBody.velocity = Vector3.zero;
                rigidBody.angularVelocity = Vector3.zero;
                rigidBody.position = this.transform.position;
               // transform.rotation = Quaternion.Euler(Vector3.zero);
            }
        }
        else // if it's inactive, rigidbody shenanigans
        { 
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;
            rigidBody.position = this.transform.position;
            // transform.rotation = Quaternion.Euler(Vector3.zero);
        }
    }

    public void GoToHelipad()
    {
            graph.AStar(currentNode, wps[32]);
            currentWaypointIndex = 0; // reset this after everytime
    }

    public void GoToRuins()
    {

            graph.AStar(currentNode, wps[21]);
            currentWaypointIndex = 0;

    }

    public void GoToFactory()
    {
 
            graph.AStar(currentNode, wps[15]);
            currentWaypointIndex = 0;
        
    }

    public void GoToTwinMountains()
    {

            this.graph.AStar(currentNode, wps[25]);
            currentWaypointIndex = 0;
        
    }

    public void GoToBarracks()
    {

            graph.AStar(currentNode, wps[4]);
            currentWaypointIndex = 0;
        
    }

    public void GoToCommandCenter()
    {
  
            graph.AStar(currentNode, wps[5]);
            currentWaypointIndex = 0;
        
    }

    public void GoToOilPumps()
    {
 
            graph.AStar(currentNode, wps[34]);
            currentWaypointIndex = 0;
        
    }

    public void GoToTankers()
    {

            graph.AStar(currentNode, wps[17]);
            currentWaypointIndex = 0;
        
    }

    public void GoToRadar()
    {

            graph.AStar(currentNode, wps[36]);
            currentWaypointIndex = 0;
        
    }

    public void GoToCommandPost()
    {

            graph.AStar(currentNode, wps[35]);
            currentWaypointIndex = 0;
        
    }

    public void GoToMiddleMap()
    {

            graph.AStar(currentNode, wps[6]);
            currentWaypointIndex = 0;
        
    }
}
